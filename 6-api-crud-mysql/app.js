'use strict';

const path = require('path');

const express = require('express');
const pug = require('pug');
const bodyParser = require('body-parser'); // Dependencia para procesar formularios
const favicon = require('serve-favicon')(path.resolve(__dirname, './public/favicon.png'));
const publicDir = express.static(path.resolve(__dirname, './public'));
const viewDir = path.resolve(__dirname, './views');
const port = process.env.PORT || 3000;
const mysql = require('mysql');
const myConnection = require('express-myconnection');
const dbOptions = {
  host: 'localhost',
  user: 'root',
  password: '',
  port: 3306,
  database: 'indentation_war',
};

const conn = myConnection(mysql, dbOptions, 'request');


let app = express();
// Configuraciones
app.set('views', viewDir);
app.set('view engine', 'pug');
app.set('port', port);
// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(publicDir);
app.use(favicon);
app.use(conn);

app.get('/', (req, res, next) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM team', (err, data) => {
      if (!err) {
        res.render('index', {
          title: 'Indentation War',
          data: data,
        });
      }
    });
  });
});

app.post('/', (req, res, next) => {
  req.getConnection((err, conn) => {
    let contacto = {
      id: null,
      name: req.body.name,
      twitter: req.body.twitter,
      country: req.body.country,
      side: req.body.side,
    };
    conn.query("INSERT INTO team SET ?", contacto, (err, data) => {
      if (!err) {
        res.redirect('/');
      } else {
        res.redirect('/agregar');
      }
    });
  });
});

app.get('/agregar', (req, res, next) => {
  res.render('add', {
    title: 'Agregar contacto',
  });
});

app.get('/editar/:id', (req, res, next) => {
  let id = req.params.id;
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM team WHERE id = ?', id, (err, data) => {
      if (!err) {
        res.render('edit', {
          title: 'Editar contacto',
          data: data,
        });
      }
    });
  });
});

app.post('/actualizar/:id', (req, res, next) => {
  req.getConnection((err, conn) => {
    let contacto = {
      id: req.body.id,
      name: req.body.name,
      twitter: req.body.twitter,
      country: req.body.country,
      side: req.body.side,
    };

    conn.query("UPDATE team SET ? WHERE id = ?", [contacto, contacto.id], (err, data) => {
      if (!err) {
        res.redirect('/');
      } else {
        res.redirect('/editar/:id');
      }
    });
  });
});

app.post('/eliminar/:id', (req, res, next) => {
  req.getConnection((err, conn) => {
    let id = req.params.id;
    conn.query('DELETE FROM team WHERE id = ?', id, (err, data) => {
      if (!err) {
        res.redirect('/');
      } else {
        return next(new Error('Registro no encontrado'));
      }
    });
  });
});

app.use((req, res, next) => {
  let err = new Error();
  err.status = 404;
  err.statusText = 'NOT FOUND';

  res.render('error', { error: err });
});

app.listen(app.get('port'), () => {
  console.log('Iniciando API CRUD con Express y MySQL');
});

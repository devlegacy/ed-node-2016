export default {
  html: [
    {
      title: 'Maratón',
      template: './index.pug',
      filename: './index.html',
      excludeAssets: [/dompdf/], //[/sw\.js/] // exclude style.js or style.[chunkhash].js
    },
    {
      title: 'Blog',
      template: './blog.pug',
      filename: './blog.html',
      excludeAssets: [/dompdf/], //[/sw\.js/] // exclude style.js or style.[chunkhash].js
    },
  ],
  sass: [
    {
      filename: 'app',
    },
    {
      filename: 'dompdf',
    },
  ],
}

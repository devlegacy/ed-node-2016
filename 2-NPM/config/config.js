
import dir from './dir.config';
import paths from './path.config';
import files from './files.config';
import rules from './webpack/rules/rules';
import sass from './webpack/sass.config';
import plugins from './webpack/plugins/plugins';
import server from './webpack/server.conf';
const config = (env, args) => {
  let sassConf = sass(env, args);
  let path = paths(env, args);
  //console.log(sassConf.plugins)
  return {
    context: dir.src,// string (absolute path!)
    // the home directory for webpack
    // the entry and module.rules.loader option
    //   is resolved relative to this directory
    devtool: (env.env === 'prod') ? 'none' : 'cheap-module-eval-source-map', // enum
    // enhance debugging by adding meta info for the browser devtools
    // source-map most detailed at the expense of build speed.
    entry: { // string | object | array
      // Here the application starts executing
      // and webpack starts bundling
      app: ['babel-polyfill', './js/script.js'],
    },
    output: {
      // options related to how webpack emits results
      path: dir.public, // string
      // the target directory for all output files
      // must be an absolute path (use the Node.js path module)
      publicPath: path.publicPath, // string
      filename: 'assets/js/[name].js', // for multiple entry points
      chunkFilename: 'assets/js/chunks/[name].js', // the filename template for additional chunks
    },
    module: {
      rules: [].concat(rules(env, args)).concat(sassConf.rules),
    },
    plugins: [].concat(sassConf.plugins).concat(plugins(env, args)),
    devServer: server(env, args),
    stats: 'errors-only',
  }
};
export default config;

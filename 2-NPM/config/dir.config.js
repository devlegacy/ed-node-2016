import { resolve, join } from 'path'; //http://tips.tutorialhorizon.com/2017/05/01/path-join-vs-path-resolve-in-node-js/
export default {
  src: resolve(__dirname, '../src'),
  public: resolve(__dirname, '../public'),
  assets: resolve(__dirname, '../public/assets'),
  dirname: resolve(__dirname, '../'),
  resolve: resolve,
  join: join,
  __dirname: __dirname,
};

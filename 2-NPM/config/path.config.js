const paths = (env, args) => {
  return {
    publicPath: '/',
    publicFontPath: '/',
    publicImgPath: '/',
    publicMediaPath: '/',
  }
}

export default paths;

import plugin from './plugins.conf';
import files from '../../files.config';
let html = (env, args) => {
  let htmlConfig = files.html.map((html) => {
    return new plugin.HtmlWebpackPlugin(
      Object.assign(html, {
        minify: (env.env === 'prod') ? {
          collapseWhitespace: true,
          collapseInlineTagWhitespace: true,
          removeComments: true,
          removeRedundantAttributes: true,
        } : false,
      })
    );
  });
  return htmlConfig;
};
export default html;

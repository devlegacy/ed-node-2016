import BrowserSyncPlugin from 'browser-sync-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
//const ClosureCompiler = require('google-closure-compiler-js').webpack;
import ClosureCompilerPlugin from 'webpack-closure-compiler';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import DashboardPlugin from 'webpack-dashboard/plugin';
import ExamplePlugin from './ExamplePlugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import glob from 'glob-all';
import HtmlWebpackExcludeAssetsPlugin from 'html-webpack-exclude-assets-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import os from 'os';
import PurifyCSSPlugin from "purifycss-webpack";
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import webpack from 'webpack';
import WebpackMonitor from 'webpack-monitor';
import workboxPlugin from 'workbox-webpack-plugin';

export default {
  BrowserSyncPlugin,
  CleanWebpackPlugin,
  // ClosureCompiler,
  ClosureCompilerPlugin,
  CopyWebpackPlugin,
  DashboardPlugin,
  ExamplePlugin,
  ExtractTextPlugin,
  glob,
  HtmlWebpackExcludeAssetsPlugin,
  HtmlWebpackPlugin,
  OptimizeCssAssetsPlugin,
  os,
  PurifyCSSPlugin,
  UglifyJsPlugin,
  webpack,
  WebpackMonitor,
  workboxPlugin,
};

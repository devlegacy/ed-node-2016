import dir from '../../dir.config';
export default {
  //Paths to clean in public dir
  path: [
    '.cache',
    '*.xml',
    '*.json',
    '*.webapp',
    '*.png',
    '*.ico',
    '*.html',
    '*.js',
    '*.map',
    'assets',
  ],
  //Options to clean folders
  options: {
    // Absolute path to your webpack root folder (paths appended to this)
    // Default: root of your package
    root: dir.public,
    // Write logs to console.
    verbose: false,
    // Instead of removing whole path recursively,
    // remove all path's content with exclusion of provided immediate children.
    // Good for not removing shared files from build directories.
    //exclude: ['./folder'],
  },
};

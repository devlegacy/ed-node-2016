

import clean from './clean.config';
import dir from '../../dir.config';
import plugin from './plugins.conf';
import html from './html.conf';
const plugins = (env, args) => {

  let htmlPlugin = html(env, args);
  //Get command to init the script and build a absolute path
  //let comm_exec = process.env.npm_lifecycle_event;

  let pluginsDev = [
    new plugin.webpack.ProgressPlugin(),
    //new plugin.DashboardPlugin(),
    new plugin.ExamplePlugin(),
    new plugin.webpack.NoEmitOnErrorsPlugin(), // do not emit compiled assets that include errors
    new plugin.webpack.NamedModulesPlugin(),
    new plugin.webpack.HotModuleReplacementPlugin(),
    new plugin.BrowserSyncPlugin(
      // BrowserSync options
      {
        files: [
          dir.resolve(dir.src, "./**/*.pug"),
        ],
        // browse to http://localhost:3000/ during development
        host: 'localhost',
        port: 3000,
        // proxy the Webpack Dev Server endpoint
        // (which should be serving on http://localhost:3100/)
        // through BrowserSync
        proxy: 'http://localhost:9000/'
      },
      // plugin options
      {
        // prevent BrowserSync from reloading the page
        // and let Webpack Dev Server take care of this
        reload: false
      }
    )
  ];
  let pluginsProd = [
    new plugin.webpack.optimize.ModuleConcatenationPlugin(),
    new plugin.webpack.optimize.DedupePlugin(),
    new plugin.webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/]),
    new plugin.webpack.NoErrorsPlugin(),
    //Recomendación de Max
    new plugin.UglifyJsPlugin({
      uglifyOptions: {
        ie8: true,
        ecma: 5,
        sourceMap: false,
        warnings: false,
        mangle: true,
        output: {
          ascii_only: true,
          comments: false,
          beautify: false,
        },
        exclude: [/\.min\.js$/gi] // skip pre-minified libs
      },
    }),
    // new plugin.ClosureCompilerPlugin({
    //   compiler: {
    //     jar: dir.resolve(dir.__dirname, './compiler.jar'), //optional
    //     //language_in: 'ECMASCRIPT6',
    //     language_out: 'ECMASCRIPT5',
    //     compilation_level: 'ADVANCED',
    //   },
    //   concurrency: 3,
    // }),
    //https://github.com/NMFR/optimize-css-assets-webpack-plugin
    new plugin.OptimizeCssAssetsPlugin({
      cssProcessorOptions: { discardComments: { removeAll: true } }
    }),
    //https://github.com/webpack-contrib/purifycss-webpack
    new plugin.PurifyCSSPlugin({
      paths: plugin.glob.sync([
        dir.join(dir.src, "./**/*.pug"),
        dir.join(dir.src, "./**/*.html"),
        dir.join(dir.src, "./**/*.js"),
        dir.join(dir.src, "../node_modules/owl.carousel/dist/**/*.js")
      ]),
      purifyOptions: { whitelist: [".fa-github"] }
    }),
    // new plugin.WebpackMonitor({
    //   capture: true, // -> default 'true'
    //   target: './myStatsStore.json', // default -> '../monitor/stats.json'
    //   launch: true, // -> default 'false'
    //   port: 3030, // default -> 8081
    // }),

  ];

  return [
    new plugin.CleanWebpackPlugin(clean.path, clean.options),
    new plugin.webpack.DefinePlugin({
      'ENV': JSON.stringify((env.env === 'prod') ? 'production' : 'devlopment'),
      'SERVICE_URL': JSON.stringify('/')
    }),
    new plugin.HtmlWebpackExcludeAssetsPlugin(),
    //Provide plugin
    new plugin.webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
  ]
    .concat((env.env === 'prod') ? pluginsProd : pluginsDev)
    .concat(htmlPlugin);
};
export default plugins;

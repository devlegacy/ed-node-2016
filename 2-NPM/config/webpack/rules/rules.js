import paths from '../../path.config';
const rules = (env, args) => {
  let path = paths(env, args);
  let rules = [
    //JS and JSX - https://babeljs.io/docs/setup/#installation
    {
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
    },
    //Pug https://github.com/pugjs/pug-loader
    {
      test: /\.pug$/,
      use: {
        loader: 'pug-loader',
        options: {
          doctype: 'html', // Insert metadata in the Doctype
          pretty: (env.env === 'prod') ? false : true,    // Pretty for user, expand file if it is true
        }
      }
    },
    //Fonts
    {
      test: /(fonts?)+.*\.(ttf|eot|woff2?|svg)$/i,
      exclude: /imgs?|images?/,
      use: [
        {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            publicPath: path.publicFontPath,
            outputPath: 'assets/fonts/',
          }
        }
      ]
    },
    //Images
    {
      test: /\.(jpe?g|png|gif|webp|svg)$/i,
      exclude: /fonts?/,
      use: [
        {
          loader: "file-loader",
          options: {
            name: "[folder]/[name].[ext]", //[folder]
            publicPath: path.publicImgPath,
            outputPath: 'assets/img/',
          }
        },
        {
          loader: "image-webpack-loader",
          options: {
            bypassOnDebug: true
          }
        }
      ]
    },
    //Media
    {
      test: /\.(mp4|mp3|txt|xml)$/,
      use: [
        {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            publicPath: path.publicMediaPath,
            outputPath: "assets/media/"
          }
        }
      ]
    }
  ];
  return rules;
};

export default rules;

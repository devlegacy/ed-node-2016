import dir from '../dir.config';
const server = (env, args) => {
  if (env.env === 'prod') {
    return {};
  }
  return {
    contentBase: dir.public,
    publicPath: '/',
    port: 9000,
    historyApiFallback: true,  // respond to 404s with index.html
    compress: true,
    hot: true,  // enable HMR on the server
    noInfo: true, // only errors & warns on hot reload
    inline: true,
    open: false,
    openPage: '',
    stats: 'errors-only',
    clientLogLevel: 'none',
    quiet: true,
    overlay: {
      warnings: true,
      errors: true
    },
    //host: "0.0.0.0",
  };
};
export default server;

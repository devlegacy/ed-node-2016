import files from '../files.config';
import plugin from './plugins/plugins.conf';
const sass = (env, args) => {
  let rules = [];
  let plugins = [];
  files.sass.forEach((scss) => {
    if (env.env === 'prod') {
      let extractText = {};

      extractText[`extract${scss.filename}`] = new plugin.ExtractTextPlugin({
        filename: `assets/css/${scss.filename}.css`,
        allChunks: true,
      });

      rules.push(
        //SASS - https://webpack.js.org/loaders/sass-loader/
        {
          test: new RegExp(`${scss.filename}\\.scss$`, 'i'),
          use: extractText[`extract${scss.filename}`].extract({
            fallback: "style-loader",
            use: [
              {
                loader: 'css-loader',
              },
              {
                loader: "postcss-loader",
              },
              {
                loader: 'resolve-url-loader',
              },
              {
                loader: 'sass-loader',
              }
            ],
            publicPath: `/assets/css/`
          })
        },
      );
      plugins.push(
        extractText[`extract${scss.filename}`]
      );
    } else {
      rules.push(
        //SASS - https://webpack.js.org/loaders/sass-loader/
        {
          test: new RegExp(`${scss.filename}\\.scss$`, 'i'),
          use: [
            {
              loader: 'style-loader',
              options: {
                hmr: true,
                sourceMap: true,
                convertToAbsoluteUrls: true
              }
            },
            {
              loader: 'css-loader',
              options: {
                importLoaders: 2,
                sourceMap: true,
              },
            },
            {
              loader: 'resolve-url-loader',
              options: {
                sourceMap: true,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
              },
            }
          ],
        },
      );
    }
  });
  return {
    rules: rules,
    plugins: plugins,
  };
}
export default sass;

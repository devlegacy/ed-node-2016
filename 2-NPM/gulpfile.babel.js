'use strict';

import gulp from 'gulp';
import pngquant from 'imagemin-pngquant';
import imagemagickCli from 'imagemagick-cli';
import gulpLoadPlugins from 'gulp-load-plugins';
import pkg from './package.json';
import { exec } from 'child_process';
/* import imagemin from 'gulp-imagemin';
import svgmin from 'gulp-svgmin';
import webp from 'gulp-webp'; */

const $ = gulpLoadPlugins();

const dir = {
  src: '',
  dist: '',
};
const files = {

};
const opts = {

};

gulp.task('default', (done) => {
  console.log('Pug with ecs6');
  done();
});

gulp.task('img', () =>
  gulp
    .src('./src/img-pre/**/*.+(png|jpeg|jpg|gif)')
    .pipe(
      $.cache(
        $.imagemin(
          {
            progressive: true,
            interlaced: true,
            use: [pngquant()]
          })
      )
    )
    .pipe(gulp.dest('./src/img'))
    .pipe($.size({ title: 'images' }))
);

gulp.task('svg', () =>
  gulp
    .src('./src/img-pre/svg/*.svg')
    .pipe(
      $.svgmin(
        {
          plugins: [
            { convertColors: false },
            { removeAttrs: { attrs: ['fill'] } }
          ]
        }
      )
    )
    .pipe(gulp.dest('./src/img/svg'))
    .pipe($.size({ title: 'svg' }))
);

gulp.task('webp', () =>
  gulp
    .src('./src/img-pre/**/*.+(png|jpeg|jpg|gif)')
    .pipe($.webp())
    .pipe(gulp.dest('./src/img/webp'))
    .pipe($.size({ title: 'webp' }))
);

gulp.task('resize', (done) => {
  //exec("find ./src/img/ -name '*.*' -execdir sh -c \"mogrify -resize '1024>' '*.*' \" {} ;", function (err, stdout, stderr) {
  exec("sh -c \"mogrify -resize '1024>' ./src/img/**/*.* ", (err, stdout, stderr) => {
    console.log(stdout);
    console.log(stderr);
    done(err);
  });
  /**
   * Usa el comando sh (para ejecutar comandos shell) con la bandera -c
   *  internamente ejecutamos el comando mogrify con las banderas que necesitamos
   */
});

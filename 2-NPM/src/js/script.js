import '../sass/app.scss';
import $ from 'jquery';
import 'owl.carousel/dist/owl.carousel';
import WOW from 'wowjs/dist/wow';
//import '../sass/dompdf.scss';
let c = console.log;
let w = window;
let d = document;
let n = navigator;

(() => {
  const wow = new WOW.WOW();
  $(d).ready(() => {
    //c($.fn.owlCarousel);
    $('.owl-carousel').owlCarousel({
      items: 3,
      loop: true,
      autoplay: true,
      autoplayTimeout: 1000,
      autoplayHoverPause: true,
      margin: 10,
      //nav: true,
    });

    $('.nav').find('a').on('click', (e) => {
      let idEnlace = e.target.hash;
      $('html, body').animate({
        scrollTop: $(idEnlace).offset().top,
      }, 1000);
    });
  });
  wow.init();
})();

if (ENV !== 'production' && module.hot) {
  module.hot.accept();
}





let button = document.getElementById('click-me');
let otherButton = document.getElementById('other-click-me');
button.addEventListener('click', () => {
  import(/*webpackChunkName: "modules/click" */ './modules/click')
    .then(data => data.default)
    .then(click => click());
});

otherButton.addEventListener('click', async () => {
  const otherclick = await import(/* webpackChunkName: "modules/otherclick" */ './modules/otherclick.js');
  otherclick.default();
});

(() => {
  c(`Hellos from webpack, init ENV: ${ENV}`);
})();

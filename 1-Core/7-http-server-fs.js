'use strict';

let c = console.log;

const http = require('http').createServer(webServer);
const fs = require('fs');

function webServer(request, response) {
  response.writeHead(200, { 'Content-Type': 'text/html' });
  fs.readFile('./assets/index.html', (err, data) => err ? c(err.message) : response.end(data));
}

http.listen(3000, 'localhost', () => { c('Servidor corriendo en http://localhost:3000/'); });

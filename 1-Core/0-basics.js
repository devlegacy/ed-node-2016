/**
 * Organización de código JS
 * Librerias / Módulos
 * Constantes
 * Objetos / Variables
 * Funciones
 * Eventos
 * Ejecuciones
 *
 *
 * Uso de camelCase
 * Cuando una instracción tenga una dola palabra, esta va en minúscula p.e. require()
 * Sólo las clases rompen esta regla, siempre van en mayúscula la letra inicial p.e. EventEmmiter()
 * Cuando una instrucción tenga dos o más palabras, apartir de la segunda la primera letra va en mayuscula p.e. createServer()
 *
 * Tipos de CamelCase
 *  UpperCamelCase
 *    Date()
 *    EventEmmiter()
 *  lowerCamelCase
 *    getElementById()
 *    createServer()
 */
'use strict'; //Modo estricto para programar con buenas practivas

let c = console.log;

c('Hola mundo desde Node.js');
c(2 + 5);


c(global);

setInterval(() => {
  c(`Hola Node.js ${new Date()}`);
}, 1000);


// Control + C detine los procesos ejecutados por la terminal de node

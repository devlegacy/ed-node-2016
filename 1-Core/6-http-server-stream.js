'use strict';

let c = console.log;

const http = require('http').createServer(webServer);
const fs = require('fs'); //File sistem
const index = fs.createReadStream('./assets/index.html');

function webServer(request, response) {
  response.writeHead(200, { 'Content-Type': 'text/html' });
  index.pipe(response);
}

http.listen(3000, 'localhost', () => { c('Servidor corriendo en http://localhost:3000/'); });

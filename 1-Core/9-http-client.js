'use strict';
let c = console.log;
const http = require('http');
const options = {
  host: 'jonmircha.com',
  port: 80,
  path: '/'
};

http
  .get( options, response => c(`El sitio ${options.host} ha respondido. Código de estado: ${response.statusCode}`))
  .on('error', (err) => c(`El sitio ${options.host} no respondió. Código de estado: ${err.message}`));

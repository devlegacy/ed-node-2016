/**
 * Streams
 *
 * 'Chorros' de información que se transmiten en 'pedazos' (Chunks)
 * Tres tipos: Lectura / Escritura / Duplex
 * Instancias de EventEmmiter
 * Acceso asíncrono
 * Es raro crear streams directamente
 * Pero muchos recursos nos ofrecen esta interfaz
 * Detrás de muchos mecanismo de Node.JS
 *  stdin / stdout
 *  request de HTTP
 *  Sockets
 *  Manipulación de ficheros / imágenes
 */
'use strict';

const fs = require('fs');
let c = console.log;
let readStream = fs.createReadStream('./assets/names.txt');
let writeStream = fs.createWriteStream('./assets/cp_names.txt');

readStream.pipe(writeStream);

readStream
  .on('data', (chunk) => {
    c(chunk);
    c(chunk.length);
    c(chunk.toString());
    c(`He leído: ${chunk.length} caracteres`);
  })
  .on('end', () => { c('Terminó la lectura del archivo'); })
  .on('error', (err) => c('Ocurrió un error', err.message));

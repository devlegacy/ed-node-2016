'use strict';

const http = require('http').createServer(webServer);
const fs = require('fs');
const path = require('path');
const url = require('url');
let c = console.log;
const urls = [
  {
    id: 1,
    route: '',
    html: './assets/index.html'
  },
  {
    id: 2,
    route: 'acerca',
    html: './assets/acerca.html'
  },
  {
    id: 3,
    route: 'contacto',
    html: './assets/contacto.html'
  },
];

function webServer(request, response) {
  let pathURL = path.basename(request.url);
  let id = url.parse(request.url, true).query.id;
  c(`PATH: ${pathURL}, id: ${id}`);
  urls.forEach((pos) => {
    if (pos.route == pathURL || pos.id == id) {
      response.writeHead(200, { 'Content-Type': 'text/html' });
      fs.readFile(pos.html, (err, data) => (err) ? c(err.message) : response.end(data));
    }
  });
}

http.listen(3000, 'localhost', () => { c('Servidor corriendo en http://localhost:3000');});

'use strict';

let c = console.log;

const http = require('http');

function webServer(request, response) {
  response.writeHead(200, { 'Content-Type': 'text/html' });
  response.end('<h1>Hola Node.JS en la web</h1>');
}

http
  .createServer(webServer)
  .listen(3000, 'localhost', () => {
    c('Servidor corriendo en: http://localhost:3000/');
  });

'use strict';

const EventEmitter = require('events').EventEmitter;
const eventEmitter = new EventEmitter();
let c = console.log;

eventEmitter
  .on('myevent', message => c(message))
  .once('myevent', message => c(`Se emite la primera vez: ${message}`));

eventEmitter.emit('myevent','Soy un emisor de eventos');
eventEmitter.emit('myevent','Soy un emisor de eventos 2');
eventEmitter.removeAllListeners('myevent');
eventEmitter.emit('myevent','Soy un emisor de eventos 3');

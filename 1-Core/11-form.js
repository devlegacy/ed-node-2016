'use strict';

const http = require('http').createServer(webServer);
const form = require('fs').readFileSync('./assets/form.html');
const queryString = require('querystring'); //Analizar ruta en la barra de direcciones
const util = require('util');
const c = console.log;
let dataString = '';

function webServer(request, response) {
  if (request.method == 'GET') {
    response.writeHead(200, { 'Content-Type': 'text/html' });
    response.end(form);
  }

  if (request.method == 'POST') {
    request
      .on('data', (data) => dataString += data)
      .on('end', () => {
        let dataObject = queryString.parse(dataString);
        let dataJSON = util.inspect(dataObject);
        let html = `
          <p>Los datos que enviaste por POST como string son: ${dataString}</p>
          <p>Los datos que enviaste por POST como JSON son: ${dataJSON}</p>
        `;
        c(html);
        response.end(html);
      });
  }
}

http.listen(3000, 'localhost', () => { c('Servidor corriendo en http://localhost:3000/'); });

/**
 * Buffers
 *  Una tira de bytes (datos binarios)
 *  Similar a un array de enteros
 *  Tamaño fijo
 *  Manipula datos directamente
 *    Sockets
 *    Streams
 *    Implementa protocolos complejos
 *    Manipula ficheros / imágenes
 *    Criptografía
 */
'use strict';

let c = console.log;
let buffer = new Buffer(26);
c(
  buffer,
  buffer.length,
  buffer.toString()
);

for (let i = 0; i < buffer.length; i++) {
  buffer[i] = i + 97;
}

c(buffer.toString());

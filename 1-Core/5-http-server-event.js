'use strict';

let c = console.log;

const http = require('http').createServer();

function webServer(request, response) {
  response.writeHead(200, { 'Content-Type': 'text/html' });
  response.end('<h1>Hola Node.JS en la web como emisor de eventos</h1>');
}

http
  .on('request', webServer)
  .on('error', (err) => c('Ocurrio un error', err.message))
  .listen(3000, 'localhost', () => { c('Servidor corriendo http://localhost:3000'); });

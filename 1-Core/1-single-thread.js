'use strict';
let c = console.log;

function singleThread() {
  /*
  ** [0] reservado por nodejs
  ** [1] reservado por nodejs
  */
  process.argv[2] = 'Estamos aprendiendo Node.JS';
  process.argv[3] = 19;
  process.argv[4] = null;
  process.argv[5] = true;

  c('----------------------------------------');
  c('        PROCESOS DE NODE.JS             ');
  c(`ID del proceso............${process.pid}`);
  c(`Título....................${process.title}`);
  c(`Directorio de Node.JS.....${process.execPath}`);
  c(`Directorio actual.........${process.cwd()}`);
  c(`Versión de Node.JS........${process.version}`);
  c(`Versiones de dependencias.${process.versions}`);
  c(`Plataforma (S.O.).........${process.platform}`);
  c(`Arquitectura (S.O.).......${process.arch}`);
  c(`Tiempo activo de Node.JS..${process.uptime()}`);
  c(`Argumento de procesos.....${process.argv}`);
  c('----------------------------------------');

  for(let i in process.argv){
    c(`[${i}] - ${process.argv[i]}`);
  }
}

singleThread();

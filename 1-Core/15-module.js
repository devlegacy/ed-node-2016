/**
 * Módulos (require / exports)
 *  require(<paquete o ruta>)
 *    Importar módulos (paquetes, otros ficheros)
 *    Garantia: Una única vez
 *    Devuelve el módulo
 *
 *  exports.propiedadPublica = <valor>
 *    El otro lado del mecanismo
 *    Se puede exportar cualquier valor
 */
'use strict';

const Clock = require('./Clock');
let myData = require('./my-data');
let c = console.log;
// let cucu = new Clock;
// cucu.theTime();
c(myData);

'use strict';

const http = require('http');
const c = console.log;
const options = {
  host: 'www.google.com.mx',
  port: 80,
  //path: '/cursos'
};

let htmlCode = '';

function httpClient(response) {
  c(`El sitio ${options.host} ha respondido. Código de estado: ${response.statusCode}`);
  response.on('data', (data) => {
    htmlCode += data;
    c(data, data.toString());
  });
}

function httpError(err) {
  c(`El sitio ${options.host} no respondió. Código de estado: ${err.code}. Mensaje de error: ${err.message}`);
}

function webServer(request, response) {
  response.writeHead(200, { 'Content-Type': 'text/html' });
  response.end(htmlCode);
}

//Instancia del cliente HTTP
http
  .get(options, httpClient)
  .on('error', httpError);

//Instancia del servidor de HTTP
http
  .createServer(webServer)
  .listen(3000, 'localhost', () => { c('Servidor corriendo en http://localhost:3000/'); });

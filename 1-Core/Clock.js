'use strict';

const EventEmitter = require('events').EventEmitter;
const inherits = require('util').inherits;

class Clock {
  constructor() {
    setInterval(() => this.theTime(), 1000);
  }
  theTime() {
    let date = new Date();
    let hour = date.toLocaleTimeString();

    console.log(hour);
  }
}

module.exports = Clock;

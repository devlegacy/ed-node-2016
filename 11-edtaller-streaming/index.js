const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;
const path = require('path');
http.listen(port, () => {
  console.log('Transmisión iniciada');
});

app
  .get('/live', (req, res, next) => res.sendFile(path.resolve(__dirname, './public/client.html')))
  .get('/', (req, res, next) => res.sendFile(path.resolve(__dirname, './public/server.html')));

io.on('connection', (socket) => {
  socket.on('streaming', (img) => {
    io.emit('watching', img);
    console.log(img);
  });
});

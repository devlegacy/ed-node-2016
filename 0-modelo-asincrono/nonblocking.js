'use strict';
let c = console.log;
const fs = require('fs');
c('Abriendo archivo \n');
let content = fs.readFile('file.txt', 'utf-8', (err, content) => {
  c(content);
});

c('\nHaciendo otra cosa \n');
c(process.uptime());

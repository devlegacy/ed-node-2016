'use strict';
let c = console.log;
const fs = require('fs');

let promise = new Promise((resolve, reject) => {
  fs.readFile('file.txt', 'utf-8', (err, content) => {
    return err ? reject(new Error('El archivo no se pudo leer')) : resolve(content);
  });
});

c('Abriendo archivo \n');

promise
  .then((data) => c(data.toString()))
  .catch((err) => c(err.message));

c('\nHaciendo otra cosa \n');
c(process.uptime());

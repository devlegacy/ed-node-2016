'use strict';

let c = console.log;
const fs = require('fs');

c('Abriendo archivo \n');

let content = fs.readFileSync('file.txt', 'utf-8');
c(content);

c('\nHianciendo otra cosa\n');
c(process.uptime());
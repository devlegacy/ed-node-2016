# Notas de la clase

## PHP Síncrono - 0.0004279613494873 seconds

Abiendo archivo...
Hola
Bienvenid@s al
Curso de Node.js
de Escuela Digital
Haciendo otra cosa
0.0004279613494873 seconds

## Node Síncrono - 0.119 seconds

Abriendo archivo

Hola
Bienvenid@s al
Curso de Node.js
de Escuela Digital

Hianciendo otra cosa

0.119

## Node asincrono - callback anonima - 0.126 seconds

Abriendo archivo


Haciendo otra cosa

0.126
Hola
Bienvenid@s al
Curso de Node.js
de Escuela Digital

## Node asincrono - callback definida - 0.119 seconds

Abriendo archivo


Haciendo otra cosa

0.119
Hola
Bienvenid@s al
Curso de Node.js
de Escuela Digital

## Node asincrono - promise - 0.142 seconds

Abriendo archivo


Haciendo otra cosa

0.142
Hola
Bienvenid@s al
Curso de Node.js
de Escuela Digital
'use strict';
let c = console.log;
const fs = require('fs');
c('Abriendo archivo \n');

function imprimir(err, content) {
  c(content);
}

let content = fs.readFile('file.txt', 'utf-8', imprimir);

c('\nHaciendo otra cosa \n');
c(process.uptime());

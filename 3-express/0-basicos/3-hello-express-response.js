const express = require('express');
const app = express();
const path = require('path');
app
  .get('/', (req, res) => {
    res.send('<h1>Hola mundo desde Express.js</h1>');
    //res.end('<h1>Hola mundo desde Express.js</h1>');
  })
  .get('/ed', (req, res) => {
    res.redirect(301, 'https://ed.team');
  })
  .get('/json', (req, res) => {
    res.json({
      name: 'Samuel',
      age: 25,
    });
  })
  .get('/render', (req, res) => {
    //Fallará por que hay que configurar el tipo de vista o plantilla que desplegará express.
    //Error: Cannot find module 'html'
    res.render(path.resolve(__dirname,'./index.html'));
  })
  .listen(3000, () => { console.log('Iniciando express.js en el puerto 3000'); });

const express = require('express');
const app = express();

app
  .get('/', (req, res) => {
    res.end('<h1>Hola mundo desde Express.js</h1>');
  })
  .listen(3000, () => { console.log('Iniciando express.js en el puerto 3000'); });

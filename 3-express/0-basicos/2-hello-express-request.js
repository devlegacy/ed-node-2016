const express = require('express');
const app = express();

app
  .get('/', (req, res) => {
    res.end('<h1>Hola mundo desde Express.js</h1>');
  })
  .get('/user/:id-:name-:age', (req, res) => {
    res.end(`
      <h1>
        ${req.params.name} bienvenid@ a Express :) tu id es: <strong>${req.params.id}</strong> y tienes ${req.params.age} a&ntilde;os
      </h1>
    `);
  })
  .get('/search', (req, res) => {
    res.end(`
      <h1>
        Bienvenido a Express, los resultados de tu b&uacute;squeda son:
        <mark>${req.query.s}</mark>
      </h1>
    `);
  })
  .listen(3000, () => { console.log('Iniciando express.js en el puerto 3000'); });

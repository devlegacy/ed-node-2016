const express = require('express');
const app = express();
const path = require('path');
app
  .get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname,'./index.html'));
  })
  .listen(3000, () => { console.log('Iniciando express.js en el puerto 3000'); });

const express = require('express');
const path = require('path');
const port = process.env.PORT || 3000;

const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const publicDir = express.static(path.resolve(__dirname, './public'));

app
  .use(publicDir)
  .get('/', (req, res) => {
    res.sendFile(`${publicDir}/index.html`);
  })
http.listen(port, () => {
  console.log('Iniciando Express y Socket.IO en http:localhost:%d', port);
});


io.on('connection', socket => {
  socket.broadcast.emit('new user', {
    message: 'Ha entrado un nuevo usuario al chat',
  });

  socket.on('new message', message => {
    io.emit('user say', message);
  });

  socket.on('disconnect', () => {
    console.log('Ha salido un usuario del chat');
    socket.broadcast.emit('bye bye user', {
      message: 'Ha salido un usuario del chat',
    });
  });
});

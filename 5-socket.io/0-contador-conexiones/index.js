/**
 * Socket.IO
 * 1 - Eventos connection y disconnect
 * 2 - Puedes crear tus propios eventos
 * 3 - emit(): Cuando se comunica un mensaje a todos los clientes conectados
 * 4 - broadcast.emit(): cuando se comunica un mensaje a todos los clientes, excepto al original
 * 5 - Los 4 puntos anteriores funcionan en el servidor y cliente
 */

const http = require('http').createServer(server);
const fs = require('fs');
const io = require('socket.io')(http);

let conexions = 0;

function server(req, res) {
  fs.readFile('./index.html', (err, data) => {
    if (err) {
      res.writeHead(500, { 'Content-type': 'text/html' });
      return res.end('<h1>Error interno de servidor</h1>');
    } else {
      res.writeHead(200, { 'Content-type': 'text/html' });
      return res.end(data, 'utf-8');
    }
  });
}

http.listen(3000, () => {
  console.log('Servidor corriendo en http://localhost:3000');
});

io.on('connection', (socket) => {
  socket.emit('hello', {
    message: 'Hola mundo son Socket.io',
  });
  conexions++;
  console.log(`Conexiones activas: ${conexions}`);
  socket.on('otro evento que me invente', (data) => {
    console.log(data);
  });


  socket.emit('connect users', {
    numbers: conexions,
  });

  socket.broadcast.emit('connect users', {
    numbers: conexions,
  });

  socket.on('disconnect', () => {
    conexions--;
    console.log(`Conexiones activas ${conexions}`);
    socket.broadcast.emit('connect users', {
      numbers: conexions
    });
  });
});

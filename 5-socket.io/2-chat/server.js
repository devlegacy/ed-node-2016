const express = require('express');
const path = require('path');
const port = process.env.PORT || 3000;

const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

const server = http.listen(port, console.log(`Iniciando el servidor Express con Socket.io en el puerto ${port}`));

const publicDir = express.static(path.resolve(__dirname, './public'));

app
  .use(publicDir)
  .get('/', (req, res, next) => {
    res.render('index');
  });

let messages = [{
  id: 1,
  text: 'Bienvenido al chat (NodeJS, Express, SockeIO)',
  nickname: 'Bot - noobot',
}];

io.on('connection', (socket) => {
  console.log(`Se ha conectado el cliente: ${socket.handshake.address}`);

  socket.emit('messages', messages);

  socket.on('add-message', (data) => {
    messages.push(data);
    io.emit('messages', messages);
  })
});

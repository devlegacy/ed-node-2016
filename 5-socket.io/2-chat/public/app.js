let d = document;
let c = console.log;
let w = window;
let n = navigator;

n.serviceWorker.register('/sw.js');
Notification.requestPermission(result => {
  c(result);
});

let io = w.io();
let frmMessage = d.getElementById('frm-message');

io.on('messages', data => {
  c(data);
  render(data);

  n
    .serviceWorker
    .ready
    .then(registration => {
      registration.showNotification('New message', {
        body: 'You have a new message',
        badge: '/icon.png',
        icon: '/background.jpeg',
        image: '/background.jpeg',
      });
    });
});

frmMessage.addEventListener('submit', (e) => {
  e.preventDefault();
  let message = {
    nickname: e.target.nickname.value,
    text: e.target.text.value,
  }
  e.target.nickname.style.display = 'none';
  c('Message to send:', message);

  io.emit('add-message', message);
});

let render = data => {
  c('New messages', data);
  let html = data.map((message, index) => {
    return `
        <div class="message">
          <strong>${message.nickname}</strong>
          <span> dice: ${message.text}</span>
        </div>
      `;
  }).join(' ');

  let messages = d.getElementById('messages');
  messages.innerHTML = html;

  messages.scrollTop = messages.scrollHeight;
};

'use strict';

const path = require('path');

const express = require('express');
const pug = require('pug');
const bodyParser = require('body-parser'); // Dependencia para procesar formularios
const morgan = require('morgan');
const restFul = require('express-method-override')('_method');
const routes = require('./routes/team-router');
const favicon = require('serve-favicon')(path.resolve(__dirname, './public/favicon.png'));
const publicDir = express.static(path.resolve(__dirname, './public'));
const viewDir = path.resolve(__dirname, './views');
const port = process.env.PORT || 3000;

let app = express();
// Configuraciones
app
  .set('views', viewDir)
  .set('view engine', 'pug')
  .set('port', port)
  // Middlewares
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: false }))
  .use(publicDir)
  .use(favicon)
  .use(morgan('dev'))
  .use(restFul)
  .use(routes);

module.exports = app;

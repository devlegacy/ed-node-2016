'use strict';

const path = require('path');

const express = require('express');
const pug = require('pug');
const bodyParser = require('body-parser'); // Dependencia para procesar formularios
const morgan = require('morgan');
const routes = require('./routes/index');
const favicon = require('serve-favicon')(path.resolve(__dirname, './public/favicon.png'));
const publicDir = express.static(path.resolve(__dirname, './public'));
const viewDir = path.resolve(__dirname, './views');
const port = process.env.PORT || 3000;

let app = express();
// Configuraciones
app
  .set('views', viewDir)
  .set('view engine', 'pug')
  .set('port', port)
  // Middlewares
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: false }))
  .use(publicDir)
  .use(favicon)
  .use(morgan('dev'))
  .use(routes);

module.exports = app;

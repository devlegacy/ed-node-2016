'use strict';

const app = require('./app');
const server = app.listen(app.get('port'), () => {
  console.log(`Iniciando API CRUD con Express y MySQL en el puerto ${app.get('port')}`);
});

'use strict';
const mysql = require('mysql');
const myConnection = require('express-myconnection');
const dbOptions = {
  host: 'localhost',
  user: 'root',
  password: '',
  port: 3306,
  database: 'indentation_war',
};

const conn = myConnection(mysql, dbOptions, 'request');

module.exports = conn;

'use strict';

const AuthController = require('../controllers/auth-controller');
const express = require('express');
const router = express.Router();
const ac = new AuthController();

router
  .get('/', ac.index)
  .get('/login', ac.logInGet)
  .post('/login', ac.logInPost)
  .get('/signin', ac.signInGet)
  .post('/signin', ac.signInPost)
  .get('/logout', ac.logOut);

module.exports = router;

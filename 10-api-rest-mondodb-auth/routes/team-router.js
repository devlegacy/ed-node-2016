'use strict';

const TeamController = require('../controllers/team-controller');
const express = require('express');
const router = express.Router();
const tc = new TeamController();

router
  .get('/teams', tc.getAll)
  .get('/agregar', tc.addForm)
  .post('/teams', tc.save)
  .get('/editar/:_id', tc.getOne)
  .put('/actualizar/:_id', tc.save)
  .delete('/eliminar/:_id', tc.delete);

module.exports = router;

'use strict';

const mongoose = require('./model');
const Schema = mongoose.Schema;
const TeamSchema = new Schema(
  {
    __id: Schema.Types.ObjectId,
    name: String,
    twitter: String,
    country: String,
    side: String,
  },
  {
    collection: 'team',
  });
const Team = mongoose.model('Team', TeamSchema);

module.exports = Team;

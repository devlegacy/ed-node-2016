'use strict';

const mongoose = require('./model');
const Schema = mongoose.Schema;
const AuthSchema = new Schema(
  {
    username: String,
    password: String,
  },
  {
    collection: 'auth'
  }
);

const Auth = mongoose.model('Auth', AuthSchema);

module.exports = Auth;

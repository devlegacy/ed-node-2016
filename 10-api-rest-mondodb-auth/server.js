'use strict';

const app = require('./app');
const server = app.listen(app.get('port'), () => {
  console.log(`Iniciando API REST MVC con Express y MySQL en el puerto ${app.get('port')}`);
});

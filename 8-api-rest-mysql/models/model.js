'use strict';
const mysql = require('mysql');
const conf = require('./db-conf');
const dbOptions = {
  host: conf.mysql.host,
  user: conf.mysql.user,
  password: conf.mysql.password,
  port: conf.mysql.port,
  database: conf.mysql.database,
};

const conn = mysql.createConnection(dbOptions);
conn.connect(err => {
  return err ? console.log(`Error al conectarse a MySQL ${err.stack}`) : console.log(`Conexión establecida con MySQL No: ${conn.threadId}`);
});
module.exports = conn;
